-module(cberlset_tests).
-include_lib("eunit/include/eunit.hrl").
-define(POOLNAME, testpool).
-define(TESTKEY, <<"testkey">>).

cberlset_test_() ->
    [{foreach, fun setup/0, fun clean_up/1,
        [fun test_encode_decode_set/0, fun test_set_item/0]}].

%%%===================================================================
%%% Setup / Teardown
%%%===================================================================

setup() ->
    cberl:start_link(?POOLNAME, 3, "localhost:8091", "test", "123456", "test"),
    cberl:remove(?POOLNAME, ?TESTKEY),
    ok.

clean_up(_) ->
    cberl:remove(?POOLNAME, ?TESTKEY),
    cberl:remove(?POOLNAME, <<"testkey1">>),
    cberl:remove(?POOLNAME, <<"notestkey">>),
    cberl:stop(?POOLNAME).

test_encode_decode_set() ->
    Keys = [<<"abc">>, <<"1">>, <<2>>],
    EncodeSet = cberlset:encode_set(Keys, <<"+">>),
    io:format("EncodeSet: ~p~n", [EncodeSet]),
    Keys1 = [<<"abc">>, <<"1">>, <<"2">>],
    EncodeSet1 = cberlset:encode_set(Keys1, <<"+">>),
    io:format("EncodeSet1: ~p~n", [EncodeSet1]),
    ?assertEqual(EncodeSet, <<43,97,98,99,32,43,49,32,43,2,32>>),
    ?assertEqual(EncodeSet1, <<"+abc +1 +2 ">>),

    {0, KeysBeforeSorted} = cberlset:decode_set(EncodeSet),
    {0, KeysBeforeSorted1} = cberlset:decode_set(EncodeSet1),

    ?assertEqual(lists:sort(Keys), lists:sort(KeysBeforeSorted)),
    ?assertEqual(lists:sort(Keys1), lists:sort(KeysBeforeSorted1)),

    Data2 = <<"+abc +1 +2 -2 ">>,
    {2, Keys2} = cberlset:decode_set(Data2),
    ?assertEqual(lists:sort(Keys2), lists:sort([<<"abc">>, <<"1">>])),

    Data3 = <<"+abc +1 +2 -3 ">>,
    {1, Keys3} = cberlset:decode_set(Data3),
    ?assertEqual(lists:sort(Keys3), lists:sort([<<"abc">>, <<"1">>, <<"2">>])),

    verify_decode(<<"+abc +1 +2 +3 -3 -2 ">>, 4, [<<"abc">>, <<"1">>]).

verify_decode(Data, Dirtyness, List) ->
    {Dirtyness, Keys} = cberlset:decode_set(Data),
    ?assertEqual(lists:sort(Keys), lists:sort(List)).

test_set_item() ->
    cberlset:modify(?POOLNAME, ?TESTKEY, 0, <<"+">>, [<<"abc">>]),
    {?TESTKEY, _, Value} = cberl:get(?POOLNAME, ?TESTKEY),
    ?assertEqual("+abc ", Value),
    Items = cberlset:item(?POOLNAME, ?TESTKEY),
    ?assertEqual([<<"abc">>], Items),

    cberlset:modify(?POOLNAME, ?TESTKEY, 0, <<"+">>, [<<"1">>]),
    {?TESTKEY, _, Value2} = cberl:get(?POOLNAME, ?TESTKEY),
    ?assertEqual("+abc +1 ", Value2),
    Items2 = cberlset:items(?POOLNAME, ?TESTKEY),
    ?assertEqual(lists:sort([<<"abc">>, <<"1">>]), lists:sort(Items2)),

    cberlset:modify(?POOLNAME, ?TESTKEY, 0, <<"+">>, [<<"2">>]),
    cberlset:modify(?POOLNAME, ?TESTKEY, 0, <<"-">>, [<<"1">>]),
    {?TESTKEY, _, Value3} = cberl:get(?POOLNAME, ?TESTKEY),
    ?assertEqual("+abc +1 +2 -1 ", Value3),
    Items3 = cberlset:items(?POOLNAME, ?TESTKEY),
    ?assertEqual(lists:sort([<<"abc">>, <<"2">>]), lists:sort(Items3)),

    cberlset:modify(?POOLNAME, ?TESTKEY, 0, <<"-">>, [<<"2">>]),
    {?TESTKEY, _, Value4} = cberl:get(?POOLNAME, ?TESTKEY),
    ?assertEqual("+abc +1 +2 -1 -2 ", Value4),
    Items4 = cberlset:items(?POOLNAME, ?TESTKEY),
    {?TESTKEY, _, Value5} = cberl:get(?POOLNAME, ?TESTKEY),
    ?assertEqual("+abc ", Value5), % the value should be compacted
    ?assertEqual(lists:sort([<<"abc">>]), lists:sort(Items4)),

    cberlset:modify(?POOLNAME, ?TESTKEY, 0, <<"-">>, [<<"3">>]),
    cberlset:modify(?POOLNAME, ?TESTKEY, 0, <<"-">>, [<<"3">>]),
    expect_value("+abc -3 -3 "),
    Items5 = cberlset:items(?POOLNAME, ?TESTKEY),
    expect_value("+abc -3 -3 "),
    ?assertEqual(lists:sort([<<"abc">>]), lists:sort(Items5)),

    cberlset:remove(?POOLNAME, ?TESTKEY, [<<"3">>]),
    cberlset:modify(?POOLNAME, ?TESTKEY, 0, <<"-">>, [<<"3">>]),
    expect_value("+abc -3 -3 -3 -3 "),
    ?assertEqual(lists:sort([<<"abc">>]), lists:sort(cberlset:items(?POOLNAME, ?TESTKEY))),
    expect_value("+abc "),

    cberlset:remove(?POOLNAME, ?TESTKEY, [<<"3">>]),
    cberlset:remove(?POOLNAME, ?TESTKEY, [<<"3">>]),
    cberlset:remove(?POOLNAME, ?TESTKEY, [<<"3">>]),
    cberlset:remove(?POOLNAME, ?TESTKEY, [<<"3">>]),
    cberlset:add(?POOLNAME, ?TESTKEY, [<<"3">>]),
    expect_value("+abc -3 -3 -3 -3 +3 "),
    ?assertEqual(lists:sort([<<"abc">>, <<"3">>]), lists:sort(cberlset:items(?POOLNAME, ?TESTKEY))),
    expect_value("+3 +abc ").

expect_value(ValueExpected) ->
    {?TESTKEY, _, Value} = cberl:get(?POOLNAME, ?TESTKEY),
    ?assertEqual(ValueExpected, Value).

