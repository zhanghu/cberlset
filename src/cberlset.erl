%%%-------------------------------------------------------------------
%%% @author zhanghu
%%% @copyright (C) 2013, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 18. 十二月 2013 下午5:13
%%%-------------------------------------------------------------------
-module(cberlset).
-author("zhanghu").

-behaviour(gen_server).

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3]).

-export([modify/5, encode_set/2, decode_set/1, item/2, items/2]).
-export([add/3, remove/3]).

-define(SERVER, ?MODULE).

-record(state, {}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link() ->
    {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
    {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term()} | ignore).
init([]) ->
    {ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
    {reply, Reply :: term(), NewState :: #state{}} |
    {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_call({set, Pool, Key, Expire, Value}, _From, State) ->
    {reply, cberl:set(Pool, Key, Expire, Value), State};
handle_call({get, Pool, Key}, _From, State) ->
    {reply, cberl:get(Pool, Key), State};
handle_call({sadd, _Pool, _Key, _Expire, _Value}, _From, State) ->
    {reply, ok, State};
handle_call({srem, _Pool, _Key, _Expire, _Value}, _From, State) ->
    {reply, ok, State};
handle_call(_Request, _From, State) ->
    {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_cast(_Request, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
    {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%
%% @doc
%%
%% @spec add(instance(), binary(), [binary()]) -> ok | {error, _}
%%
add(Pool, Key, Items) ->
    modify(Pool, Key, 0, <<"+">>, Items).

%%
%% @doc
%%
%% @spec remove(instance(), binary(), [binary()]) -> ok | {error, _}
%%
remove(Pool, Key, Items) ->
    modify(Pool, Key, 0, <<"-">>, Items).

%%%===================================================================
%%% Internal functions
%%%===================================================================

modify(Pool, Index, Expire, Op, Keys) ->
    Encoded = encode_set(Keys, Op),
    case cberl:append(Pool, 0, Index, binary_to_list(Encoded)) of
        ok ->
            Random =  erlang:trunc(random:uniform() * 100),
            if
                Random > 98 ->
                    items(Pool, Index);
                true ->
                    {ok, Index}
            end;
        Error ->
%%             io:format("modify append failed: ~p~n", [Error]),
            case Op of
                <<"+">> ->
                    case cberl:set(Pool, Index, Expire, binary_to_list(Encoded), str) of
                        ok ->
                            {ok, Index};
                        Else ->
                            Else
                    end;
                _ -> Error
            end
    end.

encode_set(Keys, Op) ->
    list_to_binary([<<Op/binary, Key/binary, <<" ">>/binary>> || Key <- Keys]).

decode_set(Data) ->
    List = binary:split(Data, [<<" ">>], [global]),
    Sets = lists:foldl(
        fun(X, Keys) ->
            if
                byte_size(X) > 1 ->
                    << Op:1/binary, Key/binary >> = X,
                    case Op of
                        <<"+">> ->
                            sets:add_element(Key, Keys);
                        <<"-">> ->
                            sets:del_element(Key, Keys);
                        _ ->
                            Keys
                    end;
                true ->
                    Keys
            end
        end,
        sets:new(), List),
    Dirtiness = length(List) - 1 - sets:size(Sets),
    {Dirtiness, sets:to_list(Sets)}.

item(Pool, Index) ->
    case cberl:get(Pool, Index) of
        {Index, _, Data} ->
            {_Dirtiness, Keys} = decode_set(list_to_binary(Data)),
            Keys;
        Error ->
            Error
    end.

% autocompact
items(Pool, Index) ->
    case cberl:get(Pool, Index) of
        {Index, _, Data} ->
            {Dirtiness, Keys} = decode_set(list_to_binary(Data)),
            if
                Dirtiness > 3 ->
                    case cberl:set(Pool, Index, 0,
                        binary_to_list(encode_set(Keys, <<"+">>)), str) of
                        ok ->
                            {ok, Index};
                        Else ->
                            Else
                    end;
                true ->
                    {ok, Keys}
            end;
        {Index, {error, Error}} ->
            {error, Error}
    end.
